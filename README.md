# Intrusion Detection System Training

&emsp;&emsp;ใน Repository นี้เป็น**เอกสารการสอนเกี่ยวกับระบบตรวจจับการบุกรุกระบบเครือข่าย** โดยมีเจตนาเอาไว้ใช้สำหรับการ**อบรมผู้เรียน** รวมถึง**เผยแพร่**ให้บุคคลทั่วไปที่สนใจศึกษาเกี่ยวกับเรื่องนี้ ข้อมูลที่เผยแพร่ใน repository นี้ได้ใช้ **Snort** ซึ่งเป็น **Open source** เกี่ยวกับ **Network Intrusion Detection System (IDS)** และได้การยอมรับทั้งในการใช้อบรม เรียนรู้ ศึกษา โดยเป็นที่รู้จักมายาวนาน รวมถึงถูก**นำไปใช้เป็น Engine** ในการ**ตรวจจับภัยคุยคาม**กับโปรดักในต่างๆ มากมาย เช่น **Cisco Firepower, Sophos UTM, McAfee Network Security** ฯลฯ

## Copyright

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

## รายละเอียดซอฟต์แวร์ที่ใช้ในการศึกษา
- VirtualBox v6.1.34
- Debian v11.3.0 NetInstall
- Snort v2.9.20 & Pulled Pork v0.7.4
- Putty v0.75

## สารบัญ 
[01. รายละเอียดเตรียมความพร้อมเบื้องต้น](docs/01-prerequisites.md)

[02. ติดตั้งระบบปฏิบัติการ Debian](docs/02-debian-install-on-virtualbox.md)

&emsp;&emsp; [02-1. คอนฟิกไอพีแอดเดรสแบบ Static](docs/02-1-ipaddress-config.md)

&emsp;&emsp; [02-2. ติดตั้งคำสั่ง sudo และกำหนดสิทธิ์ให้ผู้ใช้งาน](docs/02-2-sudo-permission.md)

&emsp;&emsp; [02-3. เปิดใช้ Non-free และ Contrib Package](docs/02-3-enable-repository-package.md)

[03. เข้าใจเกี่ยวกับระบบตรวจจับภัยคุกคามเครือข่ายเบื้องต้น](docs/03-intrusion-detection-guide.md)

[04. เรียนรู้เกี่ยวกับ Snort architecture](docs/04-snort-architecture.md)

[05. รายละเอียดในการติดตั้ง Snort V2 แบบ Binary package](docs/05-snort-v2-install-binary-package.md)

&emsp;&emsp; [05-1. คอนฟิก Pulled Pork เพื่ออัพเดต Signature](docs/05-1-pulled-pork-config.md)

[06. รายละเอียดในการติดตั้ง Snort V2 แบบ Compile source code](docs/06-snort-v2-compile-source-code.md)

[07. เรียนรู้เกี่ยวกับโหมดของ Snort ในแต่ละโหมด](docs/07-snort-modes.md)

[08. เข้าใจเกี่ยวกับการคอนฟิก snort เบื้องต้น](docs/08-snort-config-basic.md)

[09. เข้าใจเกี่ยวกับ signature snort เบื้องต้น](docs/09-snort-signature.md)

---
<br>

**LAB ในการทดสอบ**

- [ตัวอย่าง Lab 1](docs/01-labtest-exam1.md)

---
<br>

**อื่นๆ ที่เกี่ยวข้อง**

- [รูปภาพเกี่ยว TCP/IP และ Protocol Header](docs/01-other-network-layer.md)
- [ขั้นตอนการติดตั้ง docker บนระบบปฏิบัติการ Debian](docs/02-other-docker-install-on-debian.md)
- [ปรับแต่งระบบปฏิบัติการ Debian](docs/03-other-tuning-os-debian.md)
- [ลองเขียน Snort rule แบบง่ายๆ](docs/04-other-test-write-snort-rule.md)
- [รู้จักกับ Event Suppression](docs/05-other-snort-event-suppression.md)
- [ส่ง alert snort ไป syslog server](docs/06-other-snort-alert-to-syslog.md)
- [มารู้จักเครื่องมือในการทดสอบระบบ IDS](docs/07-other-tools-testids.md)
- [ตัวอย่างอุปกรณ์ที่ใช้งานร่วมกับระบบ IDS](docs/08-other-network-visibility.md)
- [โหมดเครือข่ายของ Network Adapter ใน VirtualBox](docs/09-other-virtualbox-network-adapter-types.md)
- [คอนฟิก Port Forward ของ NAT Network บน VirtualBox](docs/10-other-port-forward-nat-network.md)

---

### สุดท้าย
&emsp;&emsp;เอกสารชุดนี้ผู้เขียนเห็นว่ายังไม่มีใครนำเสนอเป็นภาษาไทยที่รายละเอียดเชิงลึกจึงได้พัฒนาเอกสารชุดนี้ขึ้นมาเผยแพร่พร้อมกับใช้ในการอบรมกับผู้เรียนที่มีความสนใจ และเพื่อให้เป็นประโยชน์กับผู้ที่ต้องการศึกษา 

#### ผู้เขียนและเผยแพร่
Burachai Pimpakorn (rokoman@gmail.com) <br>
ทีมพัฒนา SRAN (บ.โกลบอลเทคโนโลยี อินทิเกรเทด จำกัด)
