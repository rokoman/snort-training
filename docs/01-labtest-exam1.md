## ตัวอย่าง Lab 1

<br>

&emsp;<img src="/images/lab1_virtualbox_config.png" height="520">
<br>

**หมายเหตุ** <br>

&emsp;ให้ทำการสร้าง **Nat Network** โดยไปที่เมนู **File -> Preferences -> Network -> กดที่รูปการ์ดแลนที่มีเครื่องหมาย +**

&emsp;&emsp;<img src="/images/virtualbox-network-p1.png" height="320">

---
<br>

#### ขั้นตอนในการเซ็ตระบบ Lab
- ทำการ**สร้าง VM** โดยตั้งชื่อว่า **LabIDS1** และทำตามข้อ 
  - [01. รายละเอียดเตรียมความพร้อมเบื้องต้น](docs/01-prerequisites.md)
  - [02. ติดตั้งระบบปฏิบัติการ Debian](docs/02-debian-install-on-virtualbox.md)
    - [02-1. คอนฟิกไอพีแอดเดรสแบบ Static](docs/02-1-ipaddress-config.md)
    - [02-2. ติดตั้งคำสั่ง sudo และกำหนดสิทธิ์ให้ผู้ใช้งาน](docs/02-2-sudo-permission.md)
    - [02-3. เปิดใช้ Non-free และ Contrib Package](docs/02-3-enable-repository-package.md)

- ทำการ **Shutdown** เครื่อง **VM1 : LabIDS1** แล้วทำการ **Clone VM** แบบ **Full Clone** และตั้งชื่อว่า **LabIDS2**
- ทำการเปิด **VM2** ที่ชื่อ **LabIDS2** และ**แก้ไขไอพีแอดเดรส** บนขา **enp0s3** เป็น **10.0.2.16/24** บนขอ **enp0s8** เป็น **192.168.56.16/24** <br> บันทึกและรันคำสั่ง **sudo /etc/init.d/networking restart**

- ทำการ**แก้ไข**ชื่อเครื่อง **sudo nano /etc/hostname** และใส่ชื่อเครื่องว่า **labids2** บันทึกและ**รีบูตเครื่อง** (**sudo reboot**)
- หลังจาก**รีบูตเรียบร้อย** ให้ทำการ**ล็อคอิน** และติดตั้งโปรแกรมตามหัวข้อ [มารู้จักเครื่องมือในการทดสอบระบบ IDS](docs/07-other-tools-testids.md)
- ให้ทำการ**เปิด VM1 (LabIDS1)** ขึ้นมาแล้วทดลองใช้ **Putty** ในการ **SSH** เข้าไปไอพี **192.168.56.15 และ 192.168.56.16** คนละหน้าต่าง **Putty** <br>และ**ทดสอบ ping** ไปไอพี **10.0.2.16** ว่าสามารถปิงได้รึไม่
- ทำการ**ติดตั้ง Snort** ตามหัวข้อ 
  - [05. รายละเอียดในการติดตั้ง Snort V2 แบบ Binary package](docs/05-snort-v2-install-binary-package.md)
    - [05-1. คอนฟิก Pulled Pork เพื่ออัพเดต Signature](docs/05-1-pulled-pork-config.md)

- **ทดลองใช้งาน**ตามหัวข้อ
  - [07. เรียนรู้เกี่ยวกับโหมดของ Snort ในแต่ละโหมด](docs/07-snort-modes.md) <br>
  &emsp;**หมายเหตุ** ในการทดลองรันคำสั่งเวลาจะยกเลิกกด **Ctrl + C** แต่**ถ้าไม่มีทราฟฟิกจะกดออกไม่ได้** ให้ไป ping จากเครื่อง **VM2 ไปยัง VM1** เช่น ping 10.0.2.15 ฯลฯ หรือ SSH อีกหน้าต่างบนเครื่องตัวเอง ping ไปที่อื่น

- ทำการ**ติดตั้ง Snort** แบบคอมไพล์ โดย**ก่อนทำ**ให้สั่ง **sudo systemctl stop snort** และค่อยทำตามหัวข้อ 
  - [06. รายละเอียดในการติดตั้ง Snort V2 แบบ Compile source code](docs/06-snort-v2-compile-source-code.md)
  - [รู้จักกับ Event Suppression](docs/05-other-snort-event-suppression.md) และ restart service โดยสั่ง sudo systemctl restart snort-ids

- ทำการ**ติดตั้ง Docker** ตามหัวข้อ [ขั้นตอนการติดตั้ง docker บนระบบปฏิบัติการ Debian](docs/02-other-docker-install-on-debian.md)
- **เมื่อติดตั้งเสร็จ**ให้ทำการ **exit** และ**ล็อคอินเข้าใหม่**
- ทำการ**ดึง docker image** และ**รัน container เพื่อใช้สำหรับถูกสแกน** โดยคำสั่ง **docker run -d -p 80:80 -p 3306:3306 --name mutillidae citizenstig/nowasp** และทดลองเข้าหน้าเว็บ http://192.168.56.15 และให้กดตรง**เลข 3** ตรงคำพูดว่า **setup/reset the DB** และ**กด OK**
- กับไปที่ Shell และรัน sudo /opt/ids/apps/snort/bin/snort -i enp0s3 -c /opt/ids/apps/snort/etc/snort.conf -l /opt/ids/apps/snort/logs -d -N -A console
- ให้ทำการ**เปิด log** เพื่อ**ดูการโจมตี** โดยคำสั่ง **sudo tail -f /opt/ids/apps/snort/logs/alert.fast**

- **เปิดหน้าต่าง Putty** แล้ว SSH **เข้าเครื่อง VM2** และ**ทำการทดสอบ**ตามหัวข้อ [มารู้จักเครื่องมือในการทดสอบระบบ IDS](docs/07-other-tools-testids.md)
