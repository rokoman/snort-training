### รูปภาพเกี่ยว TCP/IP และ Protocol Header ต่างๆ ที่ต้องรู้

---
<br>

| รูปภาพต่างๆ |
|:-:|
| <img src="/images/network_osi_layer_tcp_ip_model.png" height="340"> |
| <br><img src="/images/network_layer_header.png" height="340"> |
| <br><img src="/images/network_data_transmission.png" height="420"> |
| <br><img src="/images/network_mtu_mss.png" height="180"> |
| <br><img src="/images/network_ethernet_header.png" height="240"> |
| <br><img src="/images/network_ipv4_header.png" height="440"> |
| <br><img src="/images/network_ipv6_header.png" height="440"> |
| <br><img src="/images/network_tcp_header.png" height="420"> |
| <br><img src="/images/network_udp_header.png" height="240"> |
| <br><img src="/images/network_icmp_header.png" height="320"> |
| <br><img src="/images/network_tcp_connection_oriented.png" height="140"> |
| <br><img src="/images/network_udp_connectionless.png" height="130"> |
| <br><img src="/images/network_3way_handshake.png" height="180"> |
| <br>**SSL/TLS Handshake**<br><br><img src="/images/network_ssl_tls_handshake.png" height="420"> |
| <br><img src="/images/network_http_protocol_stack.png" height="240">
| <br><img src="/images/network_http_protocol.png" height="210"><br>[ข้อมูลเพิ่มเติม](https://www.blognone.com/node/112422)
| <br><img src="/images/network_http_method.png" height="200"><br>[ข้อมูลเพิ่มเติม Link1](https://rithikied.medium.com/basic-http-request-part-1-4d5dbf62272)<br>[ข้อมูลเพิ่มเติม Link2](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods)
