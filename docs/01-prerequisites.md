## รายละเอียดการเตรียมความพร้อมเบื้องต้น

#### ติดตั้ง VirtualBox และ VirtualBox Extension Pack

- สามารถดาวน์โหลดได้ที่ https://www.virtualbox.org/wiki/Downloads หรือ
- https://download.virtualbox.org/virtualbox/6.1.34/VirtualBox-6.1.34-150636-Win.exe
- https://download.virtualbox.org/virtualbox/6.1.34/Oracle_VM_VirtualBox_Extension_Pack-6.1.34.vbox-extpack <br>

#### ติดตั้ง VirtualBox

---

&emsp; <img src="/images/virtualbox-p1.png" height="320">

&emsp; <img src="/images/virtualbox-p2.png" height="320">

&emsp; <img src="/images/virtualbox-p3.png" height="320">

&emsp; <img src="/images/virtualbox-p4.png" height="320">

&emsp; <img src="/images/virtualbox-p5.png" height="320">

&emsp; <img src="/images/virtualbox-p6.png" height="320">

<br>

#### ติดตั้ง VirtualBox Extension Pack

---

&emsp; <img src="/images/virtualbox-extension-p1.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p2.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p3.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p4.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p5.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p6.png" height="300">

&emsp; <img src="/images/virtualbox-extension-p7.png" height="300">

<br>

#### สร้าง Virtual Machine สำหรับติดตั้งระบบปฏิบัติการ Debian

---

- ทำการกดที่รูปคำว่า **New** ที่หน้าจอหลัก และทำการกำหนดชื่อ **VM** ที่ต้องการ เช่น **"debian_labids"** ฯลฯ <br>
ตรง **Type** ให้ทำการเลือก **Linux** และทำการเลือก **Version** เป็น **Debian (64-bit)**

&emsp;&emsp; <img src="/images/virtualbox-create-vm-p1.png" height="280">

- ทำการกำหนด **Memory size** เป็น **4096 MB** และทำการกด **Create**

&emsp;&emsp; <img src="/images/virtualbox-create-vm-p2.png" height="300">

- ทำการกำหนด **File size** เป็น **15.00 GB** และทำการกด **Create**

&emsp;&emsp; <img src="/images/virtualbox-create-vm-p3.png" height="300">

<br>

