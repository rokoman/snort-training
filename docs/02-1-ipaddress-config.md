### ตรวจสอบ IP Address && IP Gateway

---

- ล็อคอินเข้าหน้าต่าง **Terminal** ของ **VirtualBox** โดยชื่อผู้ใช้งาน **root**

&emsp;&emsp; <img src="/images/vm-debian-p1.png" height="200">

- เช็ค**ไอพีแอดเดรส**ของ VM โดยคำสั่ง **ip addr** หรือ **ip a** <br>
ซึ่งตัวอย่างจะเป็นไอพี **10.0.2.15/24**

&emsp;&emsp; <img src="/images/vm-debian-p2.png" height="210">

- เช็ค**ไอพีเกตเวย์**ของ VM โดยคำสั่ง **ip route** หรือ **ip r** <br>

&emsp;&emsp; <img src="/images/vm-debian-p3.png" height="90">
<br><br>

### คอนฟิก IP Address

---

- ทำการคอนฟิกไอพีแอดเดรส**แบบ static**

```bash
nano /etc/network/interfaces

#allow-hotplug enp0s3
#iface enp0s3 inet dhcp
auto enp0s3
iface enp0s3 inet static
    address 10.0.2.15/24
    gateway 10.0.2.2

auto enp0s8
iface enp0s8 inet static
    address 192.168.56.15/24
```

&emsp;**ทำการบันทึกและรันคำสั่ง** sudo /etc/init.d/networking restart
