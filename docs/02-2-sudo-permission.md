### ติดตั้งคำสั่ง sudo และกำหนดสิทธิ์ให้ผู้ใช้งาน

---

- ทำการอัพเดตรายการ package จาก repository และ upgrade package
- ทำการติดตั้ง sudo 

```bash
apt update
apt upgrade
apt install sudo
usermod -aG sudo snort

# ทดสอบเปลี่ยนเป็นผู้ใช้ที่ให้สิทธิ์คำสั่ง sudo
su - snort
```
**หมายเหตุ** 
 - **ชื่อผู้ใช้งาน**ที่ต้องการให้ใช้**คำสั่ง sudo** เพื่อรันคำสั่งที่**ต้องการใช้สิทธิ์ของ root** เช่น snort ฯลฯ
