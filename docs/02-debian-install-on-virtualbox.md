## ติดตั้งระบบปฏิบัติการ Debian

ดาวน์โหลดไฟล์ https://www.debian.org/download <br>
หรือ https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.3.0-amd64-netinst.iso
<br><br>


#### คอนฟิก Virtual Machine
---

- เลือก **VM** เช่น debian_labids ฯลฯ แล้วทำการกด **Settings** แล้วทำการเลือก **System -> Processor** แล้วทำการคอนฟิกเป็น **2 CPU**

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p1.png" height="320">

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p2.png" height="340">

&emsp;&emsp; **หมายเหตุ** บางเครื่องอาจมีปัญหาตอนติดตั้งเสร็จบูตขึ้นแต่ไม่สามารถใช้งานได้ ให้ทำการปิด VM ที่ลูกศร **กากบาท** <br> 
&emsp;&emsp; แล้วมา Settings แล้วติ๊ก **Enable PAE/NX** ก็จะใช้งานได้ปกติ

- เลือก **Network** ตรง **Attached to:** เลือก **NAT Network**

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p3.png" height="340">

- กด Adapter 2 แล้วทำการติ๊ก Enable Network Adapter เลือก Host-only Adapter

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p3_2.png" height="340">

- เลือก **Storage** แล้วไปที่ **Controller: IDE** กดที่รูป **CD สีฟ้า** ตรงคำว่า **Empty** แล้วกดที่รูป **CD** ข้างคำว่า **IDE Primary Device 0** <br>
แล้วกดที่ **Choose a disk file...** แล้วเลือก **ISO Debian** ที่ได้ให้ดาวน์โหลดก่อนนี้ เช่น **debian-11.3.0-amd64-netinst** ฯลฯ

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p4.png" height="340">

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p5.png" height="240">

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p6.png" height="340">

- เลือก **Audio** แล้วทำการเอาเครื่องหมายถูกออกที่ **Enable Audio**

&emsp;&emsp; <img src="/images/virtualbox-setting-vm-p7.png" height="320">
<br>

#### ติดตั้ง Debian
---

- ทำการเลือก **VM** ที่ต้องการติดตั้ง **Debian** แล้วทำการกดปุ่ม **Start** พอขึ้นหน้าจอ **Debian Installer** ให้ทำการเลือก **Install** แล้วทำการเลือก **Language: English** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p1.png" height="320">

- ทำการเลือก **Country -> other -> Asia -> Thailand** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p2.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p3.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p4.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p5.png" height="320">

- ทำการเลือก **default locate** --> **United States - en_US.UTF-8**

&emsp;&emsp; <img src="/images/vm-debian-install-p6.png" height="320">

- ทำการเลือก **Keymap** --> **American English** แล้ว **ENTER** <br>
ระบบจะทำการโหลดโปรแกรมและรับค่า **DHCP** เพื่อกำหนดไอพีแอดเดรส (IP Address) จาก VirtualBox

- ทำการกำหนดชื่อเครื่อง (**Hostname**) เช่น **labids1** ฯลฯ แล้ว **ENTER** <br>
และทำการกำหนดชื่อโดเมน (**Domain name**) เช่น **test.local** ฯลฯ แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p7.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p8.png" height="320">

- ทำการกำหนดรหัสผ่าน Root (**Root password**) เช่น **password** ฯลฯ แล้ว **ENTER** <br>
แล้วระบบจะให้ใส่รหัสผ่านอีกครั้งเพื่อตรวจสอบความถูกต้อง (verify) ของรหัสผ่าน แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p9.png" height="320">

&emsp;&emsp; **หมายเหตุ** ถ้าติตดั้งบน Production ควรกำหนดรหัสผ่านให้ปลอดภัย

- เพิ่มผู้ใช้งานเพื่อใช้งานแทน root ในกิจกรรมที่**ไม่ใช่ผู้ดูแลระบบ** โดยทำการใส่ชื่อที่ต้องการ เช่น **Administrator** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p10.png" height="320">

- กำหนด username และ password ที่ใช้ในการ login เช่น <br>
&emsp;&emsp; **Username: snort** <br>
&emsp;&emsp; **Password: password**

&emsp;&emsp; <img src="/images/vm-debian-install-p11.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p12.png" height="320">

&emsp;&emsp; **หมายเหตุ** ถ้าติตดั้งบน Production ควรกำหนดรหัสผ่านให้ปลอดภัย

- ทำการแบ่ง **Partition disks** โดยให้เลือก **Guided - use entire disk** แล้ว **ENTER** <br>
และทำการเลือก **disk** ที่ต้องการแบ่ง **partition** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p13.png" height="320">

- ทำการเลือก **Partitioning scheme: All files in one partition** แล้ว **ENTER** <br>
แล้วทำการเลือก **Finish partitioning and write changes to disk** แล้ว **ENTER** <br>
แล้วทำการเลือก **<Yes>** เพื่อทำการเขียน **partition table** ลง **disk**

&emsp;&emsp; <img src="/images/vm-debian-install-p14.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p15.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p16.png" height="320">

- ระบบจะทำการติดตั้งโปรแกรมที่จำเป็นต่างๆ เบื้องต้น

&emsp;&emsp; <img src="/images/vm-debian-install-p17.png" height="320">

- ระบบจะทำการถามว่าต้องการสแกน package อื่นๆ ใน media รึไม่ ให้ทำการเลือก **<No>** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p18.png" height="320">

- ทำการเลือก **Debian archive mirror country** เป็น **Thailand** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p19.png" height="320">

- ทำการเลือก **Debian archive mirror** เป็นแหล่งที่เก็บคลังโปรแกรมต่างๆ ในประเทศไทย <br>
เช่น **deb.debian.org** ฯลฯ แล้ว **ENTER** <br>
ระบบจะทำการคอนฟิก repository ที่เลือก และมีติดตั้งโปรแกรมบางส่วนที่ใช้งาน

&emsp;&emsp; <img src="/images/vm-debian-install-p20.png" height="320">

&emsp;&emsp; **หมายเหตุ** กรณีแหล่งที่เก็บติดต่อเครือข่ายใช้เส้นทางน้อยจะทำให้ดาวน์โหลดได้รวดเร็ว

- ระบบจะถามเพื่อขอสำรวจการใช้งาน package ต่างๆ ให้ทำการตอบ **<No>** แล้ว **ENTER**

&emsp;&emsp; <img src="/images/vm-debian-install-p21.png" height="320">

- ระบบจะถามให้เลือกติดตั้งซอฟต์แวร์เบื้องต้นในการใช้งาน ให้ทำการเลือกเฉพาะ **SSH Server** และ **standard system utilities** <br>
แล้ว **Tab** ไป **<Continue>** แล้ว **ENTER** ระบบจะทำการติดตั้งซอฟต์แวร์

&emsp;&emsp; <img src="/images/vm-debian-install-p22.png" height="320">

- ระบบจะทำการติดตั้ง GRUB เพื่อใช้ในการ boot ระบบปฏิบัติการ โดยให้ทำการเลือก **<Yes>** แล้ว **ENTER** และทำการเลือก device ที่จะติดตั้ง เช่น **/dev/sda** ฯลฯ

&emsp;&emsp; <img src="/images/vm-debian-install-p23.png" height="320">

&emsp;&emsp; <img src="/images/vm-debian-install-p24.png" height="320">

- ระบบจะทำการติดตั้ง Grub เช็ค Hardware และทำการคอนฟิกค่าเริ่มต้น แล้วให้ทำการกด **ENTER** <br>
ก็จะทำการติดตั้งระบบปฏิบัติการ Debian เป็นอันเสร็จสิ้นเรียบร้อย

&emsp;&emsp; <img src="/images/vm-debian-install-p25.png" height="320">
