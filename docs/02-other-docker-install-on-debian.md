### ขั้นตอนการติดตั้ง docker บนระบบปฏิบัติการ Debian

---
<br>

- ขั้นตอนติดตั้ง Docker
```bash
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg lsb-release -y
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io
```

- ทดสอบใช้งานเบื้องต้น
```bash
sudo docker version
sudo systemctl status docker
```

- กำหนดสิทธิ์ docker ให้กับผู้ใช้งาน
```bash
sudo usermod -aG docker snort
```
**หมายเหตุ** ตัวอย่างจะเป็นผู้ใช้งานที่ชื่อ snort
<br><br>

- ขั้นตอนติดตั้ง docker-compose
```bash
curl -s https://api.github.com/repos/docker/compose/releases/latest | grep browser_download_url | grep docker-compose-linux-x86_64 | cut -d '"' -f 4  | wget -qi -
chmod +x docker-compose-linux-x86_64
sudo mv docker-compose-linux-x86_64 /usr/bin/docker-compose
sudo chown root:root /usr/bin/docker-compose
```

- ทดสอบใช้งานเบื้องต้น
```bash
docker-compose version
```
