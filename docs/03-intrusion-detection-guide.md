## เข้าใจเกี่ยวกับระบบตรวจจับภัยคุกคามเครือข่ายเบื้องต้น

<br>

**ประเภทของระบบตรวจจับผู้บุกรุก (Types of Intrusion Detection System)**
- **Host-Based Intrusion Detection System (HIDS)** <br>
**เช่น** OSSEC, Sagan, Samhain, Wazuh ฯลฯ <br>
โดยจะใช้เทคนิคเหล่านี้ในการตรวจสอบ **File Integrity Monitoring, Log file Monitoring/Analysis, Rootkit/Malware Detection**

- **Network-Based Intrusion Detection System (NIDS)** <br>
**เช่น** Snort, Suricata, Zeek (BroIDS) ฯลฯ <br>
โดยจะใช้การวิเคราะห์ทราฟฟิกเครือข่าย **Network Traffic Analysis**

&emsp;&emsp; <img src="https://www.temok.com/blog/wp-content/uploads/2020/10/network2.jpg" height="320">

&emsp;&emsp; **หมายเหตุ** รูปภาพจาก https://www.temok.com/blog/nids-vs-hids/ <br>
<br>

---
<br>

**ตัวอย่างการติดตั้งอุปกรณ์ตรวจจับภัยคุกคามเครือข่าย (Network Diagram)** <br>

&emsp;&emsp; <img src="images/ids_installation.png" height="420"> <br>

&emsp; **คำศัพท์หรือเทคนิคที่เกี่ยวข้องกับการติดตั้ง** <br>
&emsp;&emsp; - **SPAN / Mirror Port (Port mirroring)** <br>
&emsp;&emsp;&emsp;&emsp;การทำ**สำเนาข้อมูล**แพ็กเก็ตเครือข่ายบนสวิตซ์ไปยังพอร์ตที่ต้องการวิเคราะห์ข้อมูลเครือข่าย <br>

&emsp;&emsp; - **Network Tap** <br>
&emsp;&emsp;&emsp;&emsp;อุปกรณ์ที่ใช้สำหรับติตตั้ง**วางขวาง**และทำ**สำเนาข้อมูล**ไปยังอุปกรณ์วิเคราะห์ข้อมูลเครือข่าย <br>
&emsp;&emsp;&emsp;&emsp;**คำอื่นๆที่เกี่ยวข้อง** : Copper Tap, Fiber Tap, Port/Tap Aggregator, Bypass Switch ฯลฯ

&emsp;&emsp; - **Network Packet Broker** <br>
&emsp;&emsp;&emsp;&emsp;อุปกรณ์ที่ใช้**รวบรวมข้อมูลแพ็กเก็ต**จากอุปกรณ์ **Network Tap / Switch** <br> &emsp;&emsp;&emsp;&emsp;เพื่อ**วิเคราะห์แยกแยะ กรอง และนำส่งข้อมูลแพ็กเก็ต**ไปยังอุปกรณ์วิเคราะห์ข้อมูลเครือข่าย <br>

&emsp;&emsp; - **Inline** <br>
&emsp;&emsp;&emsp;&emsp;การติดตั้งอุปกรณ์ IDS แบบระหว่างอุปกรณ์ เช่น **คั่นกลางระหว่างอุปกรณ์ Firewall กับ Switch** ฯลฯ <br> 
&emsp;&emsp;&emsp;&emsp;ซึ่งอุปกรณ์ IDS ที่วางขวางจะไม่มี IP (**Transparent**) บนขาพอร์ตที่วางขวาง <br>
&emsp;&emsp;&emsp;&emsp;เพื่อ**ป้องกันการโจมตีตัวอุปกรณ์ IDS** ทำงานเหมือน **Switch L2** ตัวนึง

&emsp;&emsp; - **Passive** <br>
&emsp;&emsp;&emsp;&emsp;การติดตั้งอุปกรณ์ IDS แบบไม่วางขวาง ซึ่ง**ใช้ความสามารถ**ของอุปกรณ์ **Switch, Network Tap** <br>
&emsp;&emsp;&emsp;&emsp;ในการ**สำเนาข้อมูลแพ็กเก็ต**มาให้อุปกรณ์ IDS <br>
<br>

---
<br>

**การแจ้งเตือนเหตุการณ์**
- **False Positive** (แจ้งเตือนเหตุการณ์ แต่เหตุการณ์นั้นไม่ใช่การโจมตีจริงจากผู้บุกรุก)
- **False Negative** (ไม่แจ้งเตือนเหตุการณ์เมื่อมีการเกิดโจมตีจริงจากผู้บุกรก)

&emsp;**ซึ่งเกิตจากได้หลายสาเหตุดังนี้** <br>
&emsp;&emsp;- ความเข้าใจเกี่ยวกับคอนฟิก เพราะระบบ IDS ต้องมีความเข้าใจเกี่ยวกับ TCP/IP, IT Security รวมถึงตัวระบบ IDS ที่นำมาใช้ <br>
&emsp;&emsp;- ไม่เข้าใจเกี่ยวกับระบบไอทีและเครือข่าย เช่น กลุ่มของอุปกรณ์, ระบบงานและแอปพลิเคชันต่างๆ ฯลฯ <br>
&emsp;&emsp;- รูปแบบการโจมตีไม่มีอยู่ในฐานข้อมูลการโจมตี (**Zero-Day Attack**) <br>
&emsp;&emsp;- เกิดจากผู้บุกรุกต้องการซ่อนการโจมตีจริง โดยการสร้างเหตุการณ์รูปแบบการโจมตีมากมาย ทำให้ผู้ดูแลเพิกเฉยต่อเหตุการณ์ <br>
&emsp;&emsp;- ผู้บุกรุกใช้เทคนิคในการหลบหลีกการตรวจจับของระบบตรวจจับผู้บุกรก (**IDS Evasion**) <br>

<br>

---
<br>

**ประเภทของการตรวจจับภัยคุกคาม (Types of Intrusion Detection System)**
- **Signature-Based / Rule-Based Detection** ตรวจจับโดยเปรียบเทียบกับฐานข้อมูลรูปแบบการโจมตี
- **Anomaly-Based Detection** ตรวจจับจากพฤติกรรมที่ผิดปกติ
    - **Protocol Anomaly**    ตรวจสอบความผิดปกติจากรูปแบบของโปรโตคอล
    - **Statistical Anomaly** ตรวจสอบความผิดปกติจากสถิติมาเป็นตัวชี้วัด
        - **Threshold detection**    
        - **Profile-Based detection**  
<br>

---
<br>

**ตัวอย่าง Signature-Based**
```
alert tcp any any -> 192.168.1.0/24 111 (content:"|00 01 86 a5|"; msg: "mountd access";)
```

**ตัวอย่าง Protocol anomaly detection**
```
preprocessor http_inspect_server: server default \
    http_methods { GET POST PUT SEARCH MKCOL COPY LINK DELETE TRACE TRACK CONNECT } \
    chunk_length 500000 \
    server_flow_depth 0 \
    client_flow_depth 0 \
    post_depth 65495 \
    oversize_dir_length 500 \
    max_header_length 750 \
```

**ตัวอย่าง Thresthold** ใน snort ยกเลิก thresthold (obsolete) ให้ใช้ detection_filter
```
# Old signature
alert tcp $EXTERNAL_NET any > $HOME_NET 22 (msg:"SSH Brute-Force Attempt"; flow:established,to_server; content:"SSH"; nocase; offset:0; depth:4; threshold: type both, track by_src, count 30, seconds 60; classtype:attempted-admin; sid:1000001; rev:1;)

# Last signature
alert tcp $EXTERNAL_NET any > $HOME_NET 22 (msg:"SSH Brute Force Attempt"; flow:established,to_server; content:"SSH"; nocase; offset:0; depth:4; detection_filter: track by_src, count 30, seconds 60; classtype:attempted-admin; sid:1000001; rev:1;)
```

<br>

---
