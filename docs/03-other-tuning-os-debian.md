### ปรับแต่งระบบปฏิบัติการ Debian

---
<br>

- **ปรับแต่งเกี่ยวกับ TCP Buffer และอื่นๆ**

&emsp;&emsp;**ตัวอย่าง Network Card 1/10Gbps** <br>

&emsp;&emsp;&emsp;**sudo nano /etc/sysctl.d/99-network-tuning.conf**

```
# Number of unprocessed RX packets before kernel starts dropping them, default = 1000
net.core.netdev_max_backlog = 300000

# turn TCP timestamp support off, default 1, reduces CPU use
net.ipv4.tcp_timestamps = 0
# turn SACK support off, default on
net.ipv4.tcp_sack = 0

# For Snifer Mode
# Increase size of RX socket buffer
net.core.rmem_default = 16777216
# Increase Max size of RX socket buffer
net.core.rmem_max = 33554432
# TCP buffer space pages (not bytes)
net.ipv4.tcp_mem = 1048576 4194304 33554432
# TCP read buffer in bytes kernel auto-tuning
net.ipv4.tcp_rmem = 1048576 4194304 33554432

# IF also Inline mode
# Increase size of RX socket buffer,
net.core.wmem_default = 16777216
# Increase Max size of TX socket buffer
net.core.wmem_max = 33554432
# TCP read buffer in bytes kernel auto-tuning
net.ipv4.tcp_wmem = 1048576 4194304 16777216

# Don't cache ssthresh from previous connection, turn off route metrics
net.ipv4.tcp_no_metrics_save = 1

# enable BPF JIT to speed up packet filtering
net.core.bpf_jit_enable = 1

# disable source validation
net.ipv4.conf.all.accept_local = 1
net.ipv4.conf.all.rp_filter = 0
```
&emsp;&emsp; เมื่อบันทึกไฟล์เสร็จให้ทำการรันคำสั่ง **sudo sysctl -p /etc/sysctl.d/99-network-tuning.conf**

<br>

- **คอนฟิก network interface เพื่อรองรับการดักจับข้อมูล ได้แก่ <br>**
  - **ไม่กำหนดไอพีแอดเดรส**ของอินเตอร์เฟส
  - เปิด **promiscuous mode** เพื่อรับข้อมูลทุกอย่าง
  - การ**ปรับแต่งการรับข้อมูล**ของ network card **ข้อมูลเพิ่มเติม**: [Link1](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node7.html), [Link2](https://s3.amazonaws.com/snort-org-site/production/document_files/files/000/000/067/original/packet-offloading-issues.pdf)

&emsp;&emsp;&emsp;**ตัวอย่าง interface eth1** <br>

&emsp;&emsp;&emsp;&emsp; <img src="/images/ids_install_mirroring_traffic.png" height="320">

&emsp;&emsp;&emsp;&emsp;**sudo apt install net-tools ethtool**

&emsp;&emsp;&emsp;&emsp;**sudo nano /etc/network/interfaces**

```
# The primary network interface
auto eth0
iface eth0 inet static
        address 192.168.1.200/24
        gateway 192.168.1.1

auto eth1
iface eth1 inet manual
        up ifconfig $IFACE 0.0.0.0 up
        up ip link set $IFACE promisc on
        post-up /sbin/ethtool -K $IFACE rx off tx off
        post-up /sbin/ethtool -K $IFACE gro off gso off tso off lro off
        down ip link set $IFACE promisc off
        down ifconfig $IFACE down

---

เมื่อบันทึกข้อมูลเสร็จทำการ restart network หรือ reboot
sudo /etc/init.d/networking restart  / sudo reboot

```

<br>

- **คอนฟิก network interface เพื่อการติดตั้ง Inline ระหว่างอุปกรณ์ ที่เรียกว่า Bridge Mode** <br>

&emsp;&emsp;&emsp;**ตัวอย่าง interface eth2, eth3 มาทำเป็น br0 (bridge)** <br>

&emsp;&emsp;&emsp;&emsp; <img src="/images/ids_install_inline_device.png" height="420">

```bash
sudo apt install bridge-utils
sudo nano /etc/network/interfaces 

เมื่อบันทึกข้อมูลเสร็จทำการ restart network หรือ reboot
sudo /etc/init.d/networking restart  / sudo reboot

---
# The primary network interface
auto eth0
iface eth0 inet static
        address 192.168.1.200/24
        gateway 192.168.1.1

auto eth2
iface eth2 inet manual
        up ifconfig $IFACE 0.0.0.0 up
        post-up /sbin/ethtool -K $IFACE gro off gso off tso off lro off
        down ifconfig $IFACE down

auto eth3
iface eth3 inet manual
        up ifconfig $IFACE 0.0.0.0 up
        post-up /sbin/ethtool -K $IFACE gro off gso off tso off lro off
        down ifconfig $IFACE down

auto br0
iface br0 inet manual
        up ifconfig $IFACE 0.0.0.0 up
        bridge_ports eth2 eth3
        bridge_fd 0
        bridge_stp off
        bridge_maxwait 0
```

&emsp;&emsp;&emsp; และต้องไปเพิ่ม **sysctl** เพื่ออนุญาติให้**ทราฟฟิกวิ่งผ่านได้** รวมถึง**การทำ filter ผ่าน IPTABLES** <br>
&emsp;&emsp;&emsp; **sudo nano /etc/sysctl.d/99-network-tuning.conf** และใส่ค่าด้านล่างแล้วบันทึก <br>

```
# Enable IP Forward
net.ipv4.ip_forward = 1
net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1

# Enable Bridge Netfilter
net.bridge.bridge-nf-call-arptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
```

&emsp;&emsp;&emsp; **sudo modprobe br_netfilter** <br>
&emsp;&emsp;&emsp; **sudo sysctl -p /etc/sysctl.d/99-network-tuning.conf**
