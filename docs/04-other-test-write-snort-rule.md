### ลองเขียน Snort rule แบบง่ายๆ

---
<br>

&emsp;**sudo nano /opt/ids/apps/snort/rules/local.rules**

```
alert tcp any any -> $HOME_NET 21 (msg:"FTP connection attempt"; sid:1000001; rev:1;)
alert icmp any any -> $HOME_NET any (msg:"ICMP connection attempt"; sid:1000002; rev:1;)
alert tcp any any -> $HOME_NET 80 (msg:"Web connection attempt"; sid:1000003; rev:1;)
alert tcp any any -> $HOME_NET 22 (msg:"SSH connection attempt"; sid:1000004; rev:1;) 
alert tcp $HOME_NET any -> $EXTERNAL_NET 443 (msg:"Request facebook.com"; flow:to_server,established; ssl_state:client_hello; content:"facebook.com"; fast_pattern:only; metadata:service ssl; classtype:policy-violation; sid: 1000005; rev:1)
```

&emsp;**และสั่ง sudo systemctl restart snort-ids.service**
