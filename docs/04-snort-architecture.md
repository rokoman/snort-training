# Snort Architecture (Snort 2.X.X)

&emsp;&emsp; <img src="/images/snort_architecture_v2.png" height="440">
<br>
---

#### รายละเอียดเบื้องต้น
- **Data Acquisiton** : ส่วนของการรับข้อมูลแพ็กเก็ตเครือข่ายมาว่าในรูปแบบไหน เช่น **PCAP, NFQ, IPQ, IPFW, AFPACKET, DUMP** ฯลฯ <br>
&emsp;**ข้อมูลเพิ่มเติม** : https://www.snort.org/faq/readme-daq

- **Packet Decoder** : ส่วนของถอดรหัสเพื่อดูโครงสร้างหรือรายละเอียดของแพ็กเก็ตเครือข่ายที่ได้รับ <br> 
&emsp;**ข้อมูลเพิ่มเติม** : https://www.snort.org/faq/readme-decode

- **Preprocessor** : ส่วนของการวิเคราะห์แยกแยะ (**analyze**) ข้อมูลแพ็กเก็ต (**packet**)  รวมถึงประกอบ (**reassembly**) <br>
&emsp;**ข้อมูลเพิ่มเติม** : http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node17.html

- **Detection Engine** : ส่วนของการตรวจสอบเปรียบเทียบแพ็กเก็ตกับฐานข้อมูลรูปแบบการโจมตี (**signature**) <br>
&emsp;**ข้อมูลเพิ่มเติม** : http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node27.html

- **Output** : ส่วนของการบันทึกข้อมูลและแจ้งเตือน <br>
&emsp;**ข้อมูลเพิ่มเติม** : http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node21.html
