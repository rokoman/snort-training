### คอนฟิก Pulled Pork เพื่ออัพเดต Signature

---
<br>

**รายละเอียด**
- ทำการ**เปิดไฟล์คอนฟิก** sudo nano /etc/snort/pulledpork.conf
- **ตัวแปร**ที่ต้องคอนฟิก ได้แก่
  - **rule_url** จะเป็น **url** ที่ใช้สำหรับดาวน์โหลด **signature**
    - **oinkcode** : จะเป็น **key** สำหรับเว็บไซต์ที่**ต้องลงทะเบียน**ใช้งานในการโหลด **signature**
  - **rule_path** : ตำแหน่งที่ใช้เก็บ **Rule files**
  - **local_rules** : ตำแหน่งของไฟล์ **local.rules**
  - **sid_msg** : ตำแหน่งของไฟล์ **sid_msg**
  - **sorule_path** : ตำแหน่ง **so rule**
  - **distro** : ระบบปฏิบัติการที่ใช้
  - **block_list** : ไฟล์ **ip reputation** (blacklist)
  - **enablesid** : ตำแหน่งไฟล์ที่ใช้สำหรับกำหนด **signature** ที่ต้องการใช้งาน
  - **dropsid** : ตำแหน่งไฟล์ที่ใช้สำหรับกำหนด **signature** ให้ เป็น drop (IPS)
  - **disablesid** : ตำแหน่งไฟล์ที่ใช้สำหรับกำหนด **signature** ที่ไม่ต้องการใช้งาน
  - **modifysid** : ตำแหน่งไฟล์ที่ใช้สำหรับกำหนด **signature** ที่ต้องการแก้ไข
  - **ips_policy** : ใช้สำหรับ signature จากทาง snort url แบบที่ลงทะเบียน โดยเลือกตาม**นโยบาย**ที่ใช้ตรวจจับ <br> **ได้แก่** **balanced, security, connectivity, max-detect**

&emsp;**หมายเหตุ** <br>
&emsp;&emsp;ให้ทำการใส่ **#** อันแรกกรณีที่**ไม่มี oinkcode** และอันสอง **community-rules** อันนี้จะ **false positive** เยอะ <br>
&emsp;หรือไม่ได้ทดสอบเรื่อง **performance** และ**เอา # ออก** ที่บรรทัด **emergingthreats**

---

## ตัวอย่างคอนฟิก
```bash
#rule_url=https://www.snort.org/reg-rules/|snortrules-snapshot.tar.gz|xxxxxxxxxxxxxxxxxxxxxxxxx
rule_url=https://snort.org/downloads/ip-block-list|IPBLOCKLIST|open
rule_url=https://rules.emergingthreats.net/|emerging.rules.tar.gz|open-nogpl

ignore=deleted.rules,experimental.rules,local.rules

temp_path=/tmp
rule_path=/etc/snort/snort.rules
out_path=/etc/snort/rules/
local_rules=/etc/snort/rules/local.rules
sid_msg=/etc/snort/sid-msg.map
sid_msg_version=1
sid_changelog=/var/log/sid_changes.log
sorule_path=/usr/lib/snort/snort_dynamicrules
snort_path=/usr/sbin/snort
config_path=/etc/snort/snort.conf

distro=Debian-11
block_list=/etc/snort/rules/iplists/default.blocklist
IPRVersion=/etc/snort/rules/iplists

enablesid=/etc/snort/enablesid.conf
dropsid=/etc/snort/dropsid.conf
disablesid=/etc/snort/disablesid.conf
modifysid=/etc/snort/modifysid.conf

ips_policy=security
version=0.7.4
```
<br>

---

**คำสั่งที่ใช้รัน Update Signature**

&emsp;sudo pulledpork.pl -c /etc/snort/pulledpork.conf

---
