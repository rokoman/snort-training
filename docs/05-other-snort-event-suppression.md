### รู้จักกับ Event Suppression

---
<br>

&emsp;&emsp; จะเป็นการ**ยกเลิกการแจ้งเตือนเหตุการณ์** เพราะบางเหตุการณ์เป็น**เหตุการณ์ปกติ** แต่มีการ**แจ้งเตือนจำนวนมาก** ซึ่งเราสามารถ**กำหนดเฉพาะ Generator ID และ Signature ID ไม่ให้แจ้งเตือนได้** และยังคงการตรวจสอบทราฟฟิกเอาไว้อยู่ โดย**แต่ละเครือข่ายจะไม่เหมือนกัน** **ผู้ดูแลเครือข่าย**ควรศึกษาเพื่อ**ทำความเข้าใจทราฟฟิก**ที่ใช้งานบนเครือข่ายที่ดูแล**ก่อนที่จะปิดแจ้งเตือน**

**หมายเหตุ**
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node19.html#SECTION00343000000000000000

---
<br>

&emsp;&emsp; โดยสามารถเพิ่มได้ที่ไฟล์ **threshold.conf** ตัวอย่าง **sudo nano /opt/ids/apps/snort/etc/threshold.conf** และ**อย่าลืม**ทำการ **restart snort** หรือ**รันคำสั่ง**เพื่อ**ทดสอบอีกครั้ง**

**ตัวอย่าง**
```
#(spp_sip) URI is too long
suppress gen_id 140, sig_id 3

#(http_inspect) INVALID CONTENT-LENGTH OR CHUNK SIZE
suppress gen_id 120, sig_id 8

#(http_inspect) UNKNOWN METHOD
suppress gen_id 119, sig_id 31

#(http_inspect) NO CONTENT-LENGTH OR TRANSFER-ENCODING IN HTTP RESPONSE
suppress gen_id 120, sig_id 3

#(http_inspect) DOUBLE DECODING ATTACK
suppress gen_id 119, sig_id 2

#(http_inspect) BARE BYTE UNICODE ENCODING
suppress gen_id 119, sig_id 4

#(http_inspect) JAVASCRIPT OBFUSCATION LEVELS EXCEEDS 1
suppress gen_id 120, sig_id 9

#(http_inspect) IIS UNICODE CODEPOINT ENCODING
suppress gen_id 119, sig_id 7

#(http_inspect) JAVASCRIPT WHITESPACES EXCEEDS MAX ALLOWED
suppress gen_id 120, sig_id 10

#(ssp_ssl) Invalid Client HELLO after Server HELLO Detected
suppress gen_id 137, sig_id 1

#(IMAP) Unknown IMAP4 command
suppress gen_id 141, sig_id 1

#(http_inspect) HTTP RESPONSE GZIP DECOMPRESSION FAILED
suppress gen_id 120, sig_id 6

#(http_inspect) UNESCAPED SPACE IN HTTP URI
suppress gen_id 119, sig_id 33

#(portscan) UDP Filtered Decoy Portscan
#suppress gen_id 122, sig_id 22

#(portscan) UDP Filtered Portscan
#suppress gen_id 122, sig_id 21

#(spp_sip) Empty request URI
suppress gen_id 140, sig_id 2

#(IMAP) Unknown IMAP4 response
suppress gen_id 141, sig_id 2

#(portscan) UDP Filtered Distributed Portscan
suppress gen_id 122, sig_id 24

#(spp_ssh) Challenge-Response Overflow exploit
suppress gen_id 128, sig_id 1

#(http_inspect) SIMPLE REQUEST
suppress gen_id 119, sig_id 32

#Consecutive TCP small segments exceeding threshol
suppress gen_id 119, sig_id 12

```
<br>

**ตัวอย่าง Suppress List จากบอร์ด pfsense**
- https://forum.netgate.com/topic/50708/suricata-snort-master-sid-disablesid-conf?lang=en-US
- https://github.com/cristianmenghi/pfsense-snort
