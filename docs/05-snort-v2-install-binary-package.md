## การติดตั้ง Snort V2 แบบ Binary package


**รายละเอียด**
- ติดตั้ง **Snort** ผ่าน **Debian Repository**
- กำหนดไอพี **HOME_NET** โดยจะใช้เป็น **subnet** เช่น **10.0.2.0/24** ฯลฯ หรือ**เฉพาะเครื่อง** เช่น **10.0.2.15/32** ฯลฯ ก็ได้

```bash
sudo apt install snort
sudo mkdir -p /usr/lib/snort/snort_dynamicrules
sudo mkdir -p /etc/snort/rules/iplists
sudo mkdir -p /etc/snort/preproc_rules
sudo /etc/init.d/snort restart
```
<br>

---

### ติดตั้ง Pulled Pork ใช้สำหรับอัพเดต Signature

```bash
mkdir -p ~/src
cd ~/src
wget https://github.com/shirkdog/pulledpork/archive/refs/tags/v0.7.4.tar.gz -O pulledpork-v0.7.4.tar.gz
tar xvfz pulledpork-v0.7.4.tar.gz
cd pulledpork-0.7.4/
sudo cp ./pulledpork.pl /usr/local/bin/
sudo cp etc/*.conf /etc/snort/
```
