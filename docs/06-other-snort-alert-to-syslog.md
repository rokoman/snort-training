### ส่ง alert snort ไป syslog server

---
<br>

- ทำการคอนฟิกที่ไฟล์ snort <br>

&emsp;&emsp; **sudo nano /opt/ids/apps/snort/etc/snort.conf**

```bash
# ทำการเพิ่ม หรือเอา # ออก
output alert_syslog: LOG_LOCAL7 LOG_ALERT
```
&emsp;&emsp; และทำการ**รันคำสั่งทดสอบใหม่**หรือสั่ง **restart snort** : **sudo systemctl restart snort-ids**

- ทำการคอนฟิก rsyslog <br>

```bash
echo "# Snort alert to syslog server" | sudo tee /etc/rsyslog.d/snort.conf
echo "local7.alert @192.168.1.250:514" | sudo tee -a /etc/rsyslog.d/snort.conf
sudo systemctl restart rsyslog.service
```
**หมายเหตุ**
- **@** คือ ส่ง syslog เป็นโปรโตคอล **UDP**
- **@@** คือ ส่ง syslog เป็นโปรโตคอล **TCP**
- ไอพี **192.168.1.250** เป็น**ไอพีตัวอย่าง** สามารถเปลี่ยนเป็น**ไอพีแอดเดรสอื่นได้ของ Syslog Server**
- **กรณี alert เหตุการณ์มีจำนวนมาก ควรไปปรับค่าต่อไปนี้ ที่ไฟล์ /etc/rsyslog.conf**
```
$SystemLogRateLimitInterval 10
$SystemLogRateLimitBurst 500
```
