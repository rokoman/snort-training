## รายละเอียดในการติดตั้ง Snort V2 แบบ Compile source code

- **ขั้นตอนคอมไพล์ snort**
```bash
# Install dependency
sudo apt install flex bison make gcc g++ pkg-config
sudo apt install libpcap0.8-dev libnfnetlink-dev libnetfilter-queue-dev libdumbnet-dev libpcre3-dev zlib1g-dev libluajit-5.1-dev libssl-dev liblzma-dev libnghttp2-dev libjemalloc-dev

mkdir -p ~/src
cd ~/src

# Compile libdaq 
wget https://www.snort.org/downloads/snort/daq-2.0.7.tar.gz
tar xvfz daq-2.0.7.tar.gz
cd daq-2.0.7
./configure --prefix=/opt/ids/libs/libdaq
make
sudo make install
cd ..

echo "/opt/ids/libs/libdaq/lib/daq" | sudo tee -a /etc/ld.so.conf.d/ids.conf
sudo ldconfig

echo 'PATH=$PATH:/opt/ids/libs/libdaq/bin' | sudo tee -a /etc/profile.d/libdaq.sh
sudo chmod a+x /etc/profile.d/libdaq.sh
source /etc/profile.d/libdaq.sh

# Compile snort
wget https://www.snort.org/downloads/snort/snort-2.9.20.tar.gz
tar xvfz snort-2.9.20.tar.gz
cd snort-2.9.20
./configure --prefix=/opt/ids/apps/snort --with-daq-includes=/opt/ids/libs/libdaq/include --with-daq-libraries=/opt/ids/libs/libdaq/lib
make
sudo make install


sudo mkdir -p /opt/ids/apps/snort/lib/snort_dynamicrules
sudo mkdir -p /opt/ids/apps/snort/rules/iplists/

sudo touch /opt/ids/apps/snort/rules/iplists/white_list.rules
sudo touch /opt/ids/apps/snort/rules/iplists/black_list.rules

sudo mkdir -p /opt/ids/apps/snort/{etc,logs,rules,preproc_rules}
sudo cp etc/*.config /opt/ids/apps/snort/etc/
sudo cp etc/*.conf /opt/ids/apps/snort/etc/
sudo cp etc/*.map /opt/ids/apps/snort/etc/
sudo chmod 755 /opt/ids/apps/snort/etc/

sudo cp preproc_rules/*.rules /opt/ids/apps/snort/preproc_rules/
sudo chmod 755 /opt/ids/apps/snort/preproc_rules/
cd ..

# ถ้าติดตั้งแบบ Binary ที่หัวข้อ 5 จะทำให้เวลาเรียกใช้งานจะตีกัน 
# ด้านล่างนี้ทำเฉพาะติดตั้งแบบ Compile Source เท่านั้น
echo 'PATH=$PATH:/opt/ids/apps/snort/bin' | sudo tee -a /etc/profile.d/snort.sh
sudo chmod a+x /etc/profile.d/snort.sh
source /etc/profile.d/snort.sh
```
<br>

---
<br>

- **ขั้นตอนติดตั้ง Signature จากเว็บ snort**
  - ทำการสมัคร https://www.snort.org/users/sign_up เพื่อใช้สำหรับโหลด signature
  - ทำการดาวน์โหลด และแตกไฟล์ และติดตั้ง

```bash
mkdir -p ~/src/temp
cd ~/src

# <oinkcode> เอาจากหน้าเว็บหลังจากสมัครเรียบร้อยแล้ว
wget -O snortrules-snapshot-29200.tar.gz https://www.snort.org/rules/snortrules-snapshot-29200.tar.gz?oinkcode=<oinkcode>

tar xvfz snortrules-snapshot-29200.tar.gz -C temp/
sudo cp temp/etc/sid-msg.map /opt/ids/apps/snort/etc/
sudo cp -R temp/rules/*.rules /opt/ids/apps/snort/rules/
#sudo cp -R temp/so_rules/* /opt/ids/apps/snort/so_rules/
#sudo cp temp/so_rules/precompiled/Debian-11/x86-64/2.9.20.0/*.so /opt/ids/apps/snort/lib/snort_dynamicrules/

# Blacklist IP Repulation (snort)
wget https://snort.org/downloads/ip-block-list
cat ip-block-list | sudo tee -a /opt/ids/apps/snort/rules/iplists/black_list.rules

# Blacklist IP Repulation (emergingthreats)
wget https://rules.emergingthreats.net/open-nogpl/snort-2.9.0/rules/compromised-ips.txt
cat compromised-ips.txt | sudo tee -a /opt/ids/apps/snort/rules/iplists/black_list.rules

```
<br>

---
<br>

- **ขั้นตอนดาวน์โหลด Signature ET OPEN จาก Emerging Threats** (https://rules.emergingthreats.net/)
  - สามารถดาวน์โหลด**แบบเป็นไฟล์รวมไฟล์เดียว**
  - สามารถดาวน์โหลด**แบบแยกไฟล์** และ**เลือก Include เฉพาะ Rules ที่ต้องการ**เท่านั้น

&emsp;&emsp; **ตัวอย่าง**

```bash

sudo wget -O /opt/ids/apps/snort/rules/emerging-all.rules https://rules.emergingthreats.net/open-nogpl/snort-2.9.0/emerging-all.rules

```
<br>

---
<br>

- **แก้ไขไฟล์ snort.conf เบื้องต้น**

```
ipvar HOME_NET 10.0.2.15/32
ipvar EXTERNAL_NET !$HOME_NET

var RULE_PATH /opt/ids/apps/snort/rules
var SO_RULE_PATH /opt/ids/apps/snort/so_rules
var PREPROC_RULE_PATH /opt/ids/apps/snort/preproc_rules

var WHITE_LIST_PATH /opt/ids/apps/snort/rules/iplists
var BLACK_LIST_PATH /opt/ids/apps/snort/rules/iplists

config checksum_mode: none
config flowbits_size: 2048

dynamicpreprocessor directory /opt/ids/apps/snort/lib/snort_dynamicpreprocessor/
dynamicengine /opt/ids/apps/snort/lib/snort_dynamicengine/libsf_engine.so
dynamicdetection directory /opt/ids/apps/snort/lib/snort_dynamicrules

#preprocessor normalize_ip4
#preprocessor normalize_tcp: ips ecn stream
#preprocessor normalize_icmp4
#preprocessor normalize_ip6
#preprocessor normalize_icmp6

preprocessor sfportscan: proto { all } \
   memcap { 10000000 } \
   scan_type { all } \
   sense_level { high }

preprocessor reputation: \
   memcap 500, \
   priority whitelist, \
   nested_ip inner, \
   scan_local, \
   whitelist $WHITE_LIST_PATH/white_list.rules, \
   blacklist $BLACK_LIST_PATH/black_list.rules

output alert_fast: alert.fast
#output alert_syslog: LOG_LOCAL7 LOG_ALERT

# Include emergingthreats rule
include $RULE_PATH/emerging-all.rules

include $PREPROC_RULE_PATH/preprocessor.rules
include $PREPROC_RULE_PATH/decoder.rules
include $PREPROC_RULE_PATH/sensitive-data.rules
```
<br>

```
sudo cp /opt/ids/apps/snort/etc/snort.conf /opt/ids/apps/snort/etc/snort.conf.backup
sudo cp /opt/ids/apps/snort/etc/threshold.conf /opt/ids/apps/snort/etc/threshold.conf.backup
sudo wget -O /opt/ids/apps/snort/etc/snort.conf https://gitlab.com/rokoman/snort-training/-/raw/main/config/snort.conf?inline=false
sudo wget -O /opt/ids/apps/snort/etc/threshold.conf https://gitlab.com/rokoman/snort-training/-/raw/main/config/threshold.conf?inline=false

```

---
<br>

- **คำสั่งใช้ทดสอบคอนฟิก snort.conf** <br>

&emsp;&emsp; **sudo /opt/ids/apps/snort/bin/snort -T -c /opt/ids/apps/snort/etc/snort.conf -l /opt/ids/apps/snort/logs**

&emsp;&emsp; **ผลลัพธ์ ถ้าทดสอบผ่านจะขึ้น**

```bash
Snort successfully validated the configuration!
Snort exiting
```

<br>

---
<br>

- **วิธีทำ script สำหรับ start/stop service**

&emsp;&emsp; **sudo nano /lib/systemd/system/snort-ids.service** 

&emsp;&emsp; และทำการเพิ่มด้านล่าง

```bash
[Unit]
Description=Snort NIDS service
After=syslog.target network.target

[Service]
Type=simple
ExecStart=/opt/ids/apps/snort/bin/snort -i enp0s3 -c /opt/ids/apps/snort/etc/snort.conf -l /opt/ids/apps/snort/logs -d -N -D

KillMode=process

[Install]
WantedBy=multi-user.target

```

&emsp;&emsp; เมื่อทำการบันทึกไฟล์เสร็จ
```
sudo systemctl enable snort-ids
sudo systemctl daemon-reload
sudo systemctl start snort-ids
sudo systemctl status snort-ids
```
