## เรียนรู้เกี่ยวกับโหมดของ Snort ในแต่ละโหมด

#### Snort สามารถรันทำงานแบ่งออกเป็น 3 โหมดดังนี้

| โหมด | รายละเอียด  | ข้อมูลเพิ่มเติม |
|---|---|:---:|
| **Sniffer Mode** | ใช้การ**ดักจับข้อมูลทราฟฟิก**เครือข่ายและ**แสดงผลข้อมูล TCP/IP ต่างๆ** ออกบนหน้าจอ  | [Link](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node4.html) |
| **Packet Logger Mode** | ใช้การ**ดักจับข้อมูลทราฟฟิก**เครือข่ายเหมือน **sniffer mode** และจะมีการ**บันทึก log file** เพื่อนำไปวิเคราะห์ | [Link](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node5.html) |
| **Network Intrusion Detection System** | ใช้การดักจับข้อมูลทราฟฟิกเครือข่ายและ**วิเคราะห์ตรวจสอบข้อมูลทราฟฟิก** โดยการ**เปรียบเทียบกับฐานข้อมูลภัยคุกคาม** (**Signature**) | [Link](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node6.html) |
<br>

---
<br>

### Sniffer Mode

| รายละเอียด  | คำสั่ง |
|---|---|
| ใช้แสดงข้อมูล **TCP/IP packet header** | **snort -v** |
| ใช้แสดงข้อมูล **Application Layer** | **snort -d** |
| ใช้แสดงข้อมูล **Ethernet Layer** | **snort -e** |
| ใช้แสดงข้อมูลรวม**แต่ละ Layer**ทั้งหมด | **snort -dev** |


### Packet Logger Mode
| รายละเอียด  | คำสั่ง |
|---|---|
| บันทึกข้อมูลทราฟฟิก | **snort -l ./log** |
| บันทึกข้อมูลทราฟฟิกใน**รูปแบบของ binary mode** | **snort -l ./log -b** |
| ใช้สำหรับ**อ่านไฟล์ที่บันทึก** | **snort -r [file]** |
| ใช้สำหรับ**อ่านไฟล์ที่บันทึก**และ**ฟิตเตอร์ข้อมูล** | **snort -r [File] [BPF filter]** เช่น **tcp/udp/icmp**, **udp and dst 10.10.x.x** ฯลฯ
| ใช้สำหรับ**อ่านไฟล์ที่บันทึก**และ**แสดงข้อมูล Protocol Header** | **snort -dev -r [File]** |

### Network Intrusion Detection Mode
| รายละเอียด  | คำสั่ง |
|---|---|
| ใช้สำหรับตรวจจับภัยคุกคาม | **snort -dev -l ./log -c /etc/snort/snort.conf** |
| ใช้สำหรับทดสอบคอนฟิกไฟล์ | **snort -T -l ./log -c /etc/snort/snort.conf** |

#### Output Option
| รายละเอียด  | คำสั่ง |
|---|---|
| **-A fast** | จะ**เขียนไฟล์แจ้งเตือน** (alert) ในรูปแบบ**ข้อมูลเบื้องต้น** โดยจะมีข้อมูล **timestamp, alert message, source and destination IPs/ports** |
| **-A full** | จะ**เขียนไฟล์แจ้งเตือน** (alert) ในรูปแบบ**เต็ม** **กรณีไม่กำหนด option ค่านี้จะเป็นค่าหลัก** |
| **-A unsock** | ส่งการแจ้งเตือนออกในรูปแบบ **UNIX socket** และ**โปรแกรมอื่นๆ** |
| **-A none** | **ปิดการแจ้งเตือน** |
| **-A console** | ส่งการแจ้งเตือนให้แสดงผลที่**หน้าจอคอนโซล** |

**หมายเหตุ**
- ต้องใช้ **sudo ในการรันคำสั่ง** หรือ**เปลี่ยนผู้ใช้งาน**ให้เป็น **root**

---
<br>
