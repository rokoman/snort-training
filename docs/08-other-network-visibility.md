### ตัวอย่างอุปกรณ์ที่ใช้งานร่วมกับระบบ IDS

---
<br>

การ**ติดตั้งระบบ Intrusion Detection System** (**IDS**) ซึ่งจะ**ขาดไม่ได้** คือ **อุปกรณ์ที่ทำงานร่วม** ได้แก่
- **Switch** เป็นอุปกรณ์ที่ใช้**เชื่อมต่ออุปกรณ์คอมพิวเตอร์ในเครือข่าย** โดยจะใช้ความสามารถของการทำ **Mirroring Port** คือ การ**สำเนาข้อมูลทราฟฟิก**ที่วิ่งอยู่บนเครือข่าย **ส่งไปยังพอร์ต**ที่ใช้**วิเคราะห์ตรวจสอบทราฟฟิก** โดยแต่ละยี่ห้อจะเรียก **Mirror port** หรือ **Span port** เป็นต้น 
  - **ข้อดี** : **ไม่ต้องลงทุนค่าใช้จ่าย**เพิ่ม
  - **ข้อเสีย** : **กรณีทราฟฟิกในเครือข่ายมีจำนวนมาก** ก็จะ**กระทบกับประสิทธิภาพ**กับ**การทำงาน**ของอุปกรณ์ เช่น **หน่วยประมวลผล** (CPU) ทำงานหนัก, **หน่วยความจำ** (Memory) **ไม่เพียงพอ**จนอาจทำให้**หยุดการทำงาน** (**Hang**) หรือ**ทำงานช้า**ลงได้

- **Network Taps** เป็น**อุปกรณ์เฉพาะ**ที่ออกแบบมาเพื่อการ**นำข้อมูลทราฟฟิกมาวิเคราะห์** และรองรับการ **bypass** เมื่อตัวอุปกรณ์มีปัญหาไม่ว่า**เสียหาย**หรือ**ไฟดับ**
  - **ข้อดี** : **ไม่กระทบ**กับเครือข่าย **ใช้เวลาติดตั้งน้อย**ไม่ถึง 10 วินาที
  - **ข้อเสีย** : **มีค่าใช้จ่ายในการลงทุนสูง** เพราะต้อง**ติดตั้งทุกจุด**ที่ต้องการวิเคราห์

- **Network Packet Broker** เป็น**อุปกรณ์เฉพาะ**ที่ออกแบบมาเพื่อ**รวบรวม(Collect)** จากอุปกรณ์ เช่น **Switch, Network Taps** เป็นต้น **คัดกรอง(Filter)** เช่น กรอง**เฉพาะข้อมูลโปรโตคอล**, **กรองเฉพาะไอพีหรือกลุ่มไอพี** เป็นต้น และ**นำส่งต่อ**ไปยัง**อุปกรณ์หรือเครื่องมือ**ที่ใช้**วิเคราะห์ทราฟฟิกเครือข่าย**
  - **ข้อดี** : **ประหยัดค่าใช้จ่าย**อุปกรณ์วิเคราะห์ข้อมูล เพราะต้องไป**ติดตั้งตามทุกจุด** ของ **Switch, Network Taps** **ลดการดูแล**ของผู้ดูแลและ**ประหยัดเวลา**ในคัดกรองตรวจสอบ **ประหยัดพื้นที่**ติดตั้งอุปกรณ์ และบางยี่ห้อรองรับการ**ถอดรหัส SSL/TLS** โดยการ**นำ Certificate มาใส่ที่อุปกรณ์**
  - **ข้อเสีย** : ตัวอุปกรณ์ยังคงมี**ราคาสูง** ฉะนั้นต้องวิเคราะห์**จำนวนจุด** **สถานที่ พื้นที่ติดตั้ง การเดินสายสัญญาณ** เป็นองค์ประกอบในการ**ตัดสินใจซื้อ**

---
<br>

| อุปกรณ์ | ภาพประกอบ |
|---|---|
| **Switch** | <ins>**Cisco Layer3 Switch**</ins> <br><br><br> <img src="/images/network_cisco_switch_l3.png" height="140"> <br> |
| **Network Tap** | <ins>**Ixia Copper Tap**</ins> <br><br> <img src="/images/network_ixia_copper_tap.png" height="120"> <br><br><br> <ins>**Ixia Copper Tap Port Aggregator**</ins> <br><br><img src="/images/network_ixia_copper_tap_port_aggregator.png" height="120"> <br><br><br> <ins>**Ixia Passive Fiber Tap**</ins> <br><img src="/images/network_ixia_fiber_tap.png" height="140"> <br><br>
| **Network Packet Broker** | <ins>**Ixia Vision One**</ins> <br><br> <img src="/images/network_ixia_network_packet_broker.png" height="120"> |

**ข้อมูลเพิ่มเติม**
- https://www.keysight.com/th/en/products/network-security/tls-and-ssl-decryption-and-encryption.html
- https://www.keysight.com/th/en/products/network-visibility/network-taps.html
- https://www.keysight.com/th/en/products/network-visibility/network-packet-brokers.html
- https://www.gigamon.com/products/optimize-traffic/traffic-intelligence/gigasmart/ssl-tls-decryption.html
- https://www.niagaranetworks.com/products/network-tap

**หมายเหตุ** <br>
&emsp;&emsp;**ยี่ห้ออุปกรณ์ที่นำมาแสดงไม่ได้เป็นการเชียร์ให้ซื้ออย่างใด เป็นแค่การนำเสนอว่าอะไรที่นำมาใช้กับระบบ IDS เพียงเท่านั้น โดยผู้เผยแพร่เคยมีโอกาสทดลองใช้อุปกรณ์เหล้านี้ในงานเท่านั้น**

---
<br>

**ตัวอย่างการติดตั้งอุปกรณ์ต่างๆ**

<img src="/images/network_diagram_example.png" height="480">
