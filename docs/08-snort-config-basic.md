## เข้าใจเกี่ยวกับการคอนฟิก snort เบื้องต้น

**ตัวแปรเกี่ยวกับกลุ่มไอพีแอดเดรสที่ควรสนใจ**

| ตัวแปร | รายละเอียด | ตัวอย่าง |
|---|---|---|
| **HOME_NET** | **ไอพีแอดเดรส**ของ**อุปกรณ์**หรือ **IP Subnet** ของเครือข่าย <br> เช่น **192.168.1.5/32, 192.168.1.5, 192.168.1.0/24** ฯลฯ | &emsp;**ipvar HOME_NET [10.0.0.0/8,172.16.0.0/12,192.168.0.0/16]** <br>&emsp;**ipvar HOME_NET 192.168.1.5/32** |
| **EXTERNAL_NET** | **ไอพีแอดเดรสทั้งหมด**หรือ**ตรงข้ามกับเครือข่าย** | &emsp;**ipvar EXTERNAL_NET any** <br>&emsp;**ipvar EXTERNAL_NET !$HOME_NET**|
| **DNS_SERVERS** | **ไอพีแอดเดรส**ของกลุ่ม **DNS Server** | &emsp;**ipvar DNS_SERVERS 192.168.1.10** <br>&emsp;**ipvar DNS_SERVERS $HOME_NET** |
| **SMTP_SERVERS** | **ไอพีแอดเดรส**ของกลุ่ม **Mail Server** | &emsp;**ipvar SMTP_SERVERS 192.168.1.11** <br>&emsp;**ipvar SMTP_SERVERS $HOME_NET** |
| **HTTP_SERVERS** | **ไอพีแอดเดรส**ของกลุ่ม **Web Server** | &emsp;**ipvar HTTP_SERVERS 192.168.1.12, 192.168.1.13** <br>&emsp;**ipvar HTTP_SERVERS $HOME_NET** |
| **SQL_SERVERS** | **ไอพีแอดเดรส**ของกลุ่ม **Database Server** | &emsp;**ipvar SQL_SERVERS 192.168.1.14** <br>&emsp;**ipvar SQL_SERVERS $HOME_NET** |
| **TELNET_SERVERS** | **ไอพีแอดเดรส**ของ **Server** ที่เปิดให้บริการ **TELNET** เช่น **Router** ฯลฯ | &emsp;**ipvar TELNET_SERVERS 192.168.1.2/32** <br>&emsp;**ipvar TELNET_SERVERS $HOME_NET** |
| **SSH_SERVERS** | **ไอพีแอดเดรส**ของ **Server** ที่เปิดให้บริการ **SSH** เช่น **Linux Server** ฯลฯ | &emsp;**ipvar SSH_SERVERS 192.168.1.12** <br>&emsp;**ipvar SSH_SERVERS $HOME_NET** |
| **FTP_SERVERS** | **ไอพีแอดเดรส**ของ **Server** ที่เปิดให้บริการ **FTP** เช่น **Web Server** ฯลฯ | &emsp;**ipvar FTP_SERVERS 192.168.1.13/32** <br>&emsp;**ipvar FTP_SERVERS $HOME_NET** |
| **SIP_SERVERS** | **ไอพีแอดเดรส**ของ **Server** ที่เปิดให้บริการ **SIP** เช่น **SIP Server** ฯลฯ | &emsp;**ipvar SIP_SERVERS 192.168.1.13/32** <br>&emsp;**ipvar SIP_SERVERS $HOME_NET** |

&emsp;**ข้อมูลเพิ่มเติม**
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node16.html

<br>

---
<br>

**ตัวแปรเกี่ยวกับกลุ่มพอร์ตที่ควรสนใจ**

| ตัวแปร | รายละเอียด | ตัวอย่าง |
|---|---|---|
| **HTTP_PORTS** | **พอร์ต**ของ **HTTP Server** | &emsp;**portvar HTTP_PORTS 80** |
| **SHELLCODE_PORTS** | **พอร์ต**ของ **SHEELCODE** | &emsp;**portvar SHELLCODE_PORTS !80** |
| **ORACLE_PORTS** | **พอร์ต**ของ **Oracle Database Server** | &emsp;**portvar ORACLE_PORTS 1024:** |
| **SSH_PORTS** | **พอร์ต**ของ **Secure Shell Server** เช่น **Linux Server** ฯลฯ | &emsp;**portvar SSH_PORTS [22, 2200, 9922]** |
| **ORACLE_PORTS** | **พอร์ต**ของ **Oracle Database Server** | &emsp;**portvar ORACLE_PORTS 1024:** |
| **FTP_PORTS** | **พอร์ต**ของ **FTP Server** | &emsp;**portvar FTP_PORTS [21,2100,3535]** |
| **SIP_PORTS** | **พอร์ต**ของ **SIP Server** | &emsp;**portvar SIP_PORTS [5060,5061,5600]** |

&emsp;**ข้อมูลเพิ่มเติม** 
- https://www.snort.org/faq/readme-variables

<br>

---
<br>

**ตัวแปรตำแหน่งที่เก็บไฟล์ต่างๆ**
| ตัวแปร | รายละเอียด | ตำแหน่ง |
|---|---|---|
| **RULE_PATH** | ตำแหน่งที่เก็บ **Rule files** | **/etc/snort/rules** |
| **SO_RULE_PATH** | ตำแหน่งที่เก็บ **Shared object files** | **/usr/lib/snort/so_rules** |
| **PREPROC_RULE_PATH** | ตำแหน่งที่เก็บ **Preprocessor rule file** | /**etc/snort/preproc_rules** |
| **WHITE_LIST_PATH** | ตำแหน่งที่เก็บ **White list files** | **/etc/snort/rules/iplists** |
| **BLACK_LIST_PATH** | ตำแหน่งที่เก็บ **Black list files** | **/etc/snort/rules/iplists** |

&emsp;**ข้อมูลเพิ่มเติม** 
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node16.html

<br>

---
<br>

**ตัวแปรอื่นๆ กลุ่ม decoder**
| ตัวแปร | รายละเอียด |
|---|---|
| **disable_decode_alerts** | **ยกเลิกการแจ้งเตือนเหตุการณ์เกี่ยวกับถอดรหัสโปรโตคอล** |
| **disable_tcpopt_experimental_alerts** | **ยกเลิกการแจ้งเตือนของ** TCP Options ที่เป็น**ฟีเจอร์ทดลอง** |
| **disable_tcpopt_obsolete_alerts** | **ยกเลิกการแจ้งเตือนของ** TCP Options **ที่ล้าสมัยหรือเลิกใช้แล้ว** |
| **disable_tcpopt_ttcp_alerts** | **ยกเลิกการแจ้งเตือนของ** TCP Options ที่เกี่ยวกับ T/TCP : **TCP for Transactions** |
| **disable_tcpopt_alerts** | **ยกเลิกการแจ้งเตือนของ** TCP Options **อื่นๆ** |
| **disable_ipopt_alerts** | **ยกเลิกการแจ้งเตือนของ** IP Options **ที่ไม่ถูกต้อง** |

&emsp;**ข้อมูลเพิ่มเติม** 
- https://www.snort.org/faq/readme-decode

<br>

---
<br>

**ตัวแปรอื่นๆ**
| ตัวแปร | รายละเอียด | ตัวอย่าง |
|---|---|---|
| **checksum_mode: all, none, noip, notcp, noudp, noicmp, ip, tcp, udp, icmp** **ค่าเริ่มต้น all** | **ตรวจสอบความถูกต้อง**ของโปรโตคอล | **checksum_mode:** none |
| **flowbits_size: 64** | **ขนาดสูงสุด**ของ flowbits **ที่จัดเก็บ**ไว้ **ค่าเริ่มต้น 64** | **flowbits_size: 2048** |
| **snaplen: <bytes>** | **ขนาดสุงสุด**ของข้อมูล**แพ็กเก็ต** (MTU) ที่ใช้สำหรับ**ส่งข้อมูล** **ค่าเริ่มต้น 1518** | **snaplen:** 1492, 1500, 1518

&emsp;**ข้อมูลเพิ่มเติม** 
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node16.html

<br>

---
<br>

**ตัวแปรกลุ่มของ preprocessor ต่างๆ**
| ตัวแปร | รายละเอียด |
|---|---|
| **normalize_ip4** | ช่วย**ลำดับข้อมูล**ทราฟฟิกของ **IPv4** ใช้เฉพาะการ**ติดตั้ง Inline** เพื่อป้องกันเทคนิคในการ**หลบเลี่ยงการตรวจจับภัยคุกคาม** |
| **normalize_tcp** | ช่วย**ลำดับข้อมูล**ทราฟฟิกของ **TCP** ใช้เฉพาะการ**ติดตั้ง Inline** เพื่อป้องกันเทคนิคในการ**หลบเลี่ยงการตรวจจับภัยคุกคาม** |
| **normalize_icmp4** | ช่วย**ลำดับข้อมูล**ทราฟฟิกของ **ICMP v4** ใช้เฉพาะการ**ติดตั้ง Inline** เพื่อป้องกันเทคนิคในการ**หลบเลี่ยงการตรวจจับภัยคุกคาม** |
| **normalize_ip6** | ช่วย**ลำดับข้อมูล**ทราฟฟิกของ **IPv6** ใช้เฉพาะการ**ติดตั้ง Inline** เพื่อป้องกันเทคนิคในการ**หลบเลี่ยงการตรวจจับภัยคุกคาม** |
| **normalize_icmp6** | ช่วยลำดับข้อมูลทราฟฟิกของ **ICMP v6** ใช้เฉพาะการ**ติดตั้ง Inline** เพื่อป้องกันเทคนิคในการ**หลบเลี่ยงการตรวจจับภัยคุกคาม** |
| **frag3** | การตรวจจับการ**แบ่งข้อมูลแพ็กเก็ตของไอพี**กระจายออกเป็นชิ้นๆ (**IP defragmentation**) |
| **stream5** | ใช้สำหรับการ**ติดตามทราฟฟิกของแต่ละเซลชันของการเชื่อมต่อ** (tracking session) และ**ตรวจจับความผิดปกติ** หรือที่เรียกกว่า **Stateful inspection** |
| **http_inspect** | ใช้สำหรับการ**ลำดับข้อมูล**ทราฟฟิก (**HTTP Normalization**) และตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล HTTP**
| **rpc_decode** | ใช้สำหรับการ**ลำดับข้อมูล**ทราฟฟิก (**RPC Normalization**) และตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล RPC** |
| **bo** | ใช้สำหรับ**ตรวจจับทราฟฟิก**ของโปรแกรม **Back Orifice** ที่ใช้**รีโมตควบคุมเครื่องจากระยะไกล** หรือที่เรียกกว่า**โทรจัน** (**Trojan hourse**) |
| **ftp_telnet** | ใช้สำหรับ**ลำดับข้อมูล**ทราฟฟิก (**FTP / Telnet Normalization**) และตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล ftp และ telnet** |
| **smtp** | ใช้สำหรับ**ลำดับข้อมูล**ทราฟฟิก (**SMTP Normalization**) และตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล smtp** |
| **dcerpc2** | ใช้สำหรับการ**ลำดับข้อมูล**ทราฟฟิก (**SMB / DCE-RPC Normalization**) และตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล SMB และ DCE-RPC**  |
| **dns** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล DNS** |
| **ssh** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล SSH** |
| **imap** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล IMAP** |
| **pop** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล POP** |
| **ssl** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล SSL/TLS** และ**ปล่อยผ่านทราฟฟิก** (**Traffic bypass**) |
| **sip** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล SIP** |
| **modbus** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล MODBUS** ใช้บนระบบ**เครือข่าย SCADA** |
| **dnp3** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล DNP3** ใช้บนระบบ**เครือข่าย SCADA** |
| **sfportscan** | ใช้สำหรับตรวจจับ**การสแกนพอร์ต** (Portscan) |
| **arpspoof** | ใช้สำหรับตรวจจับ**ความผิดปกติ** (**anomaly detection**) ของ**โปรโตคอล ARP** โดยการ**ปลอมแปลงตัวเองเป็นอุปกรณ์อื่น**ผ่านโปรโตคอล ARP หรือที่เรียกเทคนิคนี้ว่า **ARP Spoof** |
| **sensitive_data** | ใช้สำหรับตรวจจับ**ข้อมูลที่อ่อนไหว** (**sensitive data**) เช่น **เลขบัตรประชาชน (ID Card), เลขบัตรเครดิต (Credit card) ฯลฯ** |
| **reputation** | ใช้สำหรับตรวจจับไอพีแอดเดรสที่อยู่ในฐานข้อมูลไม่ปลอดภัย หรือที่เรียกว่า **Blacklist IP** |

&emsp;**ข้อมูลเพิ่มเติม** 
- https://www.snort.org/faq/readme-normalize
- https://www.snort.org/faq/readme-frag3
- https://www.snort.org/faq/readme-stream5
- https://www.snort.org/faq/readme-dcerpc2
- https://www.snort.org/faq/readme-ftptelnet
- https://www.snort.org/faq/readme-dns
- https://www.snort.org/faq/readme-dnp3
- https://www.snort.org/faq/readme-reputation
- https://www.snort.org/faq/readme-sfportscan
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node17.html
- https://www.cisco.com/c/en/us/td/docs/security/firepower/601/configuration/guide/fpmc-config-guide-v601/Transport___Network_Layer_Preprocessors.html
- https://blog.talosintelligence.com/2009/12/require3whs-and-mystery-of-four-way.html
- https://www.oreilly.com/library/view/snort-cookbook/0596007914/ch04.html
- https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/029/original/Inline_Normaliation_with_Snort_2.9.0.pdf
- https://www.cisco.com/c/en/us/support/docs/security/firesight-management-center/117927-technote-firesight-00.html
<br>

---
<br>
