### โหมดเครือข่ายของ Network Adapter ใน VirtualBox

---
<br>

**Network Modes ประกอบไปด้วย**
- **Not Attached**
- **NAT** (**ค่าเริ่มต้นใช้งาน**)
- **NAT Network**
- **Bridged Adapter**
- **Internal Network**
- **Host-only Adapter**

---
<br>

| อุปกรณ์ | ภาพประกอบ |
|---|---|
| **NAT** (**ค่าเริ่มต้นใช้งาน**) | <img src="/images/virtualbox_network_mode_nat.png" height="280"> |
| **NAT Network** | <br><img src="/images/virtualbox_network_mode_network_nat.png" height="280"> |
| **Bridged Adapter** | <br><img src="/images/virtualbox_network_mode_bridge.png" height="220"> |
| **Internal Network** | <br><img src="/images/virtualbox_network_mode_internal_network.png" height="300"> |
| **Host-only Adapter** | <br><img src="/images/virtualbox_network_mode_host_only_adapter.png" height="320"> |

**รูปภาพจาก** : https://www.nakivo.com/blog/virtualbox-network-setting-guide/
