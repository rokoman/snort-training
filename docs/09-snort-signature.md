## เข้าใจเกี่ยวกับ signature snort เบื้องต้น

**โดยจะประกอบแบ่งออกเป็น 2 ส่วน**
- **Rule Header (ส่วนหัวของ Rule)**
  - **Action**
    - **alert** : ทำการ**แจ้งแตือนตามรูปแบบที่คอนฟิก**ไว้และ**บันทึกข้อมูล**แพ็คเกต
    - **log** : ทำการ**บันทึกข้อมูล**แพ็คเกต
    - **pass** : ทำการ**ปล่อยผ่าน**แพ็คเกต
    - **drop** : ทำการ**บล็อกแพ็คเกต**และบันทึกข้อมูล
    - **reject** : ทำการ**บล็อกแพ็คเกต** โดยส่ง**ทราฟฟิกเพื่อแจ้งยกเลิกทราฟฟิก**และบันทึกข้อมูล
      - ถ้าเป็นโปรโตคอล **TCP** จะทำการส่ง **TCP reset**
      - ถ้าเป็นโปรโตคอล **UDP** จะทำการส่งข้อความ **ICMP port unreachable**

    - **sdrop** : ทำการ**บล็อกแพ็คเกต** แต่**ไม่ทำการบันทึกข้อมูล**
  - **Protocol (โปรโตคอล)** <br>
  &emsp;&emsp; **TCP, UDP, ICMP, IP (ปัจจุบันที่รองรับ)**

  - **Source Address (ไอพีแอดเดรสต้นทาง)**
  - **Source Port (พอร์ตต้นทาง)**
  - **Direction (ทิศทางของทราฟฟิก)**
    - Traffic from internal host -> outbound : **$HOME_NET any -> $EXTERNAL_NET any**
    - Traffic from external host -> inbound : **$EXTERNAL_NET -> $HOME_NET any**
    - Can be bidirectional by using <> : **$EXTERNAL_NET any <> $HOME_NET any**

  - **Destination Address (ไอพีแอดเดสปลายทาง)**
  - **Destination Port (พอร์ตปลายทาง)**

- **Rule Options (ส่วนประกอบของ Rule ในการตรวจจับ)**
  - **msg (ข้อความที่แจ้งเตือนและบันทึกข้อมูล)**
  - **reference (ข้อมูลอ้างอิงเกี่ยวกับ signature นี้)**
  - **sid (เลขไอดีของ signature)**
    - **sid < 100 (สงวนไว้ใช้ในอนาคต)**
    - **sid 100-999,999 (ใช้สำหรับ signature ที่เผยแพร่จากทาง snort)**
    - **sid >= 1,000,000 (ใช้สำหรับผู้อื่น)**
    - **อื่นๆ** เช่น **signature** จาก **Emerging Threats** ก็จะมี**จัดสรรนอกเหนือ**เช่นกัน ดูเพิ่มเติม [Link](https://doc.emergingthreats.net/bin/view/Main/SidAllocation)

  - **rev (ตัวเลขเวอร์ชั่นของการแก้ไข signature)**
  - **classtype (หมวดหมู่ของ signature)** ดูเพิ่มเติม [Link](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node31.html#SECTION00446000000000000000)

  - **priority (ลำดับความสำคัญ)**
    - **High (1)**
    - **Medium (2)**
    - **Low (3)**
    - **Very Low (4)**
<br>

---
<br>

&emsp;&emsp; <img src="/images/snort_rule_syntax.png" height="260">

---
<br>

**เกี่ยวกับการเปิดใช้งาน Rule โดยค่าเริ่มต้น เฉพาะที่โหลดจากทาง snort แบ่งออกเป็น 4 ประเภทหลัก**
- **Connectivity over Security (Connectivity)**
  - **Criteria (เกณฑ์ในการตรวจจับ):**
     1. **CVSS Score** = 10
     2. **CVE** year is current - 2 ตัวอย่างเช่น 2022, 2021, 2020 ฯลฯ
     
- **Balanced (Balanced)**
  - **Criteria (เกณฑ์ในการตรวจจับ):**
     1. **CVSS Score** >= 9
     2. **CVE** year is current - 2 ตัวอย่างเช่น 2022, 2021, 2020 ฯลฯ
     3. **MALWARE-CNC** rules
     4. **EXPLOIT-KIT** rules
     5. **SQL Injection** rules
     6. **Blacklist** rules
     7. **Includes** the rule in the **Connectivity over Security** policy

- **Security over Connectivity (Security)**
  - **Criteria (เกณฑ์ในการตรวจจับ):**
     1. **CVSS Score** >= 8
     2. **CVE** year is current - 3 ตัวอย่างเช่น 2022, 2021, 2020, 2019 ฯลฯ
     3. **MALWARE-CNC** rules
     4. **EXPLOIT-KIT** rules
     5. **SQL Injection** rules
     6. **Blacklist** rules
     7. **App-detect** rules
     8. **Includes** the rule in the **Connectivity over Security** and **Balanced** policy

- **Maximum Detection (max-detect)**
  - **Criteria (เกณฑ์ในการตรวจจับ):**
     1. The coverage is required for in field testing
     2. **Includes** rules in the **Security, Balanced, and Connectivity** rule sets.
     3. Includes all **active rules** above **Sid:10000**, **unless otherwise specified**.

**ข้อมูลเพิ่มเติม**
- https://www.snort.org/faq/why-are-rules-commented-out-by-default

---
<br>

**แหล่ง Signature ต่างๆ**
- **Snort Community** (ฟรี**ไม่ต้องลงทะเบียน** ลิงค์อยู่ในหน้า **Downloads เว็บไซต์ snort.org** แต่ **False Positive สูง** และ**ไม่มีการทดสอบ Performance**)
- **Snort Register Account** (ฟรีต้อง**ลงทะเบียนผ่านเว็บไซต์ snort.org** โดย **signature** จะออก**ช้ากว่า**ตัว **subscription** ประมาณ **7-30 วัน**)
- **Snort Subscription Account** (**จ่ายตังส์ผ่านการสมัครเว็บไซต์ snort.org**)
- **Emerging Threats** : **ET OPEN** (ฟรี**ไม่ต้องลงทะเบียน** ลิงค์ https://rules.emergingthreats.net/)
- **Emerging Threats** : **ET PRO** (**จ่ายตังส์** บริษัท Proofpoint ลิงค์ https://www.proofpoint.com/us/threat-insight/et-pro-ruleset)
- **อื่นๆ** ตาม**เว็บไซต์ต่างๆ** หรือ **Blog ด้าน Cyber Security** เช่น **ABUSEch** (https://abuse.ch) ฯลฯ

---
<br>

**ตัวอย่างของ Signature**

```
alert tcp 192.168.1.0/24 any -> $[EXTERNAL_NET] 25 (content: "hack"; msg: "malicious packet"; sid:2000001;)

alert tcp $EXTERNAL_NET $FILE_DATA_PORTS -> $HOME_NET any (msg:"BROWSER-IE Microsoft Internet Explorer htmlFile ActiveX control universal XSS attempt"; flow:to_client,established; file_data; content:"<iframe"; content:".ActiveXObject"; distance:0; content:"new "; within:500; content:"htmlFile"; within:100; content:".setTimeout("; within:100; content:"document.open()"; distance:0; content:"new "; within:500; content:"htmlFile"; within:100; content:"parentWindow.setTimeout("; within:500; fast_pattern; metadata:policy balanced-ips drop, policy max-detect-ips drop, policy security-ips drop, service ftp-data, service http, service imap, service pop3; reference:cve,2017-0210; classtype:attempted-user; sid:42204; rev:2;)
```

---
<br>

&emsp;&emsp; <img src="/images/snort_rule_example.png" height="360">

---
<br>

**ข้อมูลเพิ่มเติม**
- http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node27.html
- https://snort-org-site.s3.amazonaws.com/production/document_files/files/000/000/116/original/Snort_rule_infographic.pdf
- https://media.defcon.org/DEF%20CON%2025/DEF%20CON%2025%20workshops/Jack%20Mott%20and%20Jason%20Williams%20-%20Practical%20Network%20Signature%20Development%20For%20Open%20Source%20IDS/DEFCON-25-Workshop-Jack-Mott-Jason-Williams-Practical-Network-Signature-Development-For-Open-Source-IDS.pdf
