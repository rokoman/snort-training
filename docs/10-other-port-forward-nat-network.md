### คอนฟิก Port Forward ของ NAT Network (VirtualBox)

---
<br>

- ไปที่ **File -> Preferences...** ของหน้าต่าง **VirtualBox** เลือก **Network**
- เลือก **NatNetwork** แล้ว**ดับเบิ้ลคลิก** หรือกดที่**รูปเฟือง**
- คอนฟิกตามรูปภาพ

&emsp;&emsp; <img src="/images/vm-setting-port-forward-p1.png" height="300">

&emsp;&emsp; <img src="/images/vm-setting-port-forward-p2.png" height="220">

&emsp;&emsp; <img src="/images/vm-setting-port-forward-p3.png" height="260">
